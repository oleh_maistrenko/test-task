$(document).ready(function(){

//------- open/close diploma add
$('.block2_diplomaImg2').click(function(){
	$(".block2_d2window").fadeIn(500);
	var diploma_slider1 =$('.diploma_slider1');
	$('.diploma_slider1').Peppermint(); 
	$('.b2_lArr').click(diploma_slider1.data('Peppermint').prev);
	$('.b2_rArr').click(diploma_slider1.data('Peppermint').next);
});
$('.b2_d2_close').click(function(){
	$(".block2_d2window").fadeOut(500);
});
//------open/close diploma and resize name
$('.block2_diplomaImg1').click(function(){
	$(".block2_d1window").fadeIn(500);
	let h = $(".diploma1_bigImg").height()
	$(".block2_diploma_name").css({
   		'font-size' : h*0.075 + 'px',
   		'top' : h*0.62 + 'px',
   		'line-height' : h*0.075 + 'px'
	});
});
$(window).on('resize', function(){
	let h = $(".diploma1_bigImg").height()
	$(".block2_diploma_name").css({
   		'font-size' : h*0.075 + 'px',
   		'top' : h*0.62 + 'px',
   		'line-height' : h*0.075 + 'px'
	});
});
$('.b2_d1_close').click(function(){
	$(".block2_d1window").fadeOut(500);
});



//------scroll functions
function goToByScroll(c, t) {
    $('html,body').animate({
        scrollTop: $("." + c).offset().top
    }, t);
} 	
$(".block1_button").click(function(){
    goToByScroll("block6_frame", 1500);
});

$(".block6_submitBtn").click(function(){
    $(".block7").fadeIn(500);
    goToByScroll("block7", 500);
});



//--------custom select
  var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        $('.select-selected').css("color", "#000");
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);




//-----------changing checkbox labels when check/uncheck
$('.checkbox').each(function( ) {
 if($(this).find('input').prop("checked") == true){
        $(this).css("color", "black");
    }
    else {
        $(this).css("color", "gray");
    }
});
$('.checkbox').click(function(){
	if($(this).find('input').prop("checked") == true){
        $(this).css("color", "black");
    }
    else {
        $(this).css("color", "gray");
    }
});



//----------adding rows in textarea
var rowCount = 1;
$('.addressInput').bind('input propertychange', function() {
	var inp=this.value.split("\n");
	if (inp.length > rowCount) {
		rowCount++;
		$('.addressInput').prop("rows", $('.addressInput').prop("rows") + 1);
		return;
	}
	if (inp.length + 1 < $('.addressInput').prop("rows")) {
		rowCount--;
		$('.addressInput').prop("rows", $('.addressInput').prop("rows") - 1);
	}
	autosize(this);
});


});


    

